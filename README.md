# Information

Demo application integrating Jersey, Graphql-java and Spring for offering a Rest API for creating, listing and updating of recursively-defined inventories.

## Technologies
	1. Maven - Dependency management
	2. Spring Boot - Servlet engine, dependency injection, etc.
	3. JUnit, hamcreat - Testing
	4. Jersey - Web server controller
	5. Graphql-java - GraphQL Java implementation
	6. Logback - Logging

# Development

## IDE
	1. Run GQLDemoApplication.java class

## Console
	1. mvn clean package
	2. java -jar listit-1.0-SNAPSHOT.jar

# Testing
To run all tests:

	1. mvn clean test

