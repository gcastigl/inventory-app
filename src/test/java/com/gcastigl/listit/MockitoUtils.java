package com.gcastigl.listit;

import org.springframework.data.jpa.repository.JpaRepository;

import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class MockitoUtils {

	public static <T> void returnIdentityWhenSaveToRepository(JpaRepository<T, ?> repository, Class<T> clazz) {
		when(repository.save(any(clazz))).then(invocation -> invocation.getArgumentAt(0, clazz));
	}

	public static void alwaysFail() {
		assertFalse(true);
	}
}
