package com.gcastigl.listit.service.impl;

import com.gcastigl.listit.model.entity.AppUser;
import com.gcastigl.listit.model.io.AuthenticationToken;
import com.gcastigl.listit.repository.AppUserRepository;
import com.gcastigl.listit.service.api.AppUserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationServiceImplTest {

	@Mock
	private AppUserRepository repository;

	@Mock
	private AppUserService users;

	@Mock
	private PasswordEncoder encoder;

	@Spy
	@InjectMocks
	private AuthenticationServiceImpl authentications;

	@Before
	public void setup() {
		ReflectionTestUtils.setField(authentications, "secret_", "s3cr37");
		ReflectionTestUtils.setField(authentications, "hoursToExpiration_", 1L);
	}

	@Test
	public void createTokenWithValidPassword() {
		// Given
		String nickname = "test";
		String password = "123";
		String encodedPassword = "passowrd-hash";
		LocalDateTime creationDate = LocalDateTime.now();
		String signature = "token-values-signature";
		AppUser.AuthenticationType authType = AppUser.AuthenticationType.USER_AND_PASS;
		AppUser user = new AppUser(AppUser.ProfileType.REGULAR, nickname, encodedPassword, creationDate, authType);
		// When
		when(users.findByNickname(nickname, authType)).thenReturn(Optional.of(user));
		when(encoder.matches(password, encodedPassword)).thenReturn(true);
		when(encoder.encode(anyString())).thenReturn(signature);
		// Then
		AuthenticationToken token = authentications.createToken(nickname, password, authType, Optional.empty());
		assertEquals(token.getNickname(), nickname);
		assertEquals(token.getProfileType(), user.getProfileType());
		assertTrue(token.getToken().endsWith(signature));
	}

	@Test(expected = BadCredentialsException.class)
	public void restrictTokenWithInvalidPassword() {
		// Given
		String nickname = "test";
		String password = "123";
		String encodedPassword = "passowrd-hash";
		LocalDateTime creationDate = LocalDateTime.now();
		AppUser.AuthenticationType authType = AppUser.AuthenticationType.USER_AND_PASS;
		AppUser user = new AppUser(AppUser.ProfileType.REGULAR, nickname, encodedPassword, creationDate, authType);
		// When
		when(users.findByNickname(nickname, authType)).thenReturn(Optional.of(user));
		when(encoder.matches(password, encodedPassword)).thenReturn(false);
		// Then
		authentications.createToken(nickname, password, authType, Optional.empty());
	}

	@Test
	public void getAppuserFromValidSignature() {
		// Given
		String nickname = "test";
		String password = "123";
		String encodedPassword = "passowrd-hash";
		LocalDateTime creationDate = LocalDateTime.now();
		Long expiration = System.currentTimeMillis() * TimeUnit.HOURS.toMillis(1);
		String signature = "cryptographic-signature";
		AppUser.AuthenticationType authType = AppUser.AuthenticationType.USER_AND_PASS;
		AppUser user = new AppUser(AppUser.ProfileType.REGULAR, nickname, encodedPassword, creationDate, authType);
		// When
		when(users.getEncodedPassword(nickname)).thenReturn(encodedPassword);
		when(encoder.encode(anyString())).thenReturn(signature);
		when(
			encoder.matches(authentications.genRawSignature(nickname, encodedPassword, expiration, authType), signature)
		).thenReturn(true);
		when(users.findByNickname(nickname, authType)).thenReturn(Optional.of(user));
		// Then
		String token = authentications.generateToken(nickname, password, expiration, authType);
		Optional<AppUser> userFromToken = authentications.getUserFromToken(token);
		assertEquals(userFromToken.orElse(null), user);
	}
}
