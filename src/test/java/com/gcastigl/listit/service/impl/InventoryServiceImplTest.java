package com.gcastigl.listit.service.impl;

import com.gcastigl.listit.model.entity.AppUser;
import com.gcastigl.listit.model.entity.Inventory;
import com.gcastigl.listit.model.io.CreateInventoryRequest;
import com.gcastigl.listit.model.io.SyncRequest;
import com.gcastigl.listit.repository.InventoryRepository;
import com.gcastigl.listit.service.api.AppUserService;
import com.gcastigl.listit.service.api.InventoryService;
import com.gcastigl.listit.util.Either;
import com.gcastigl.listit.util.Pages;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.transaction.support.TransactionCallback;

import java.util.List;

import static com.gcastigl.listit.MockitoUtils.alwaysFail;
import static com.gcastigl.listit.MockitoUtils.returnIdentityWhenSaveToRepository;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InventoryServiceImplTest {

	@Mock
	private InventoryRepository repository;

	@Mock
	private AppUserService users;

	@Mock
	private SpringTransactionService transactions;

	@Spy
	@InjectMocks
	private InventoryServiceImpl inventories;

	@Test
	public void createRootInventory() {
		// Given
		AppUser user = mock(AppUser.class);
		String externalId = "ext_0";
		CreateInventoryRequest request = new CreateInventoryRequest(externalId, null, 0, "test title", null, false);
		// When
		returnIdentityWhenSaveToRepository(repository, Inventory.class);
		whenTransactionThenExecute();
		when(repository.findIdByOwnerAndExternalId(user, request.getExternalId())).thenReturn(null);
		// Then
		inventories.create(user, request).bimap(
			error -> alwaysFail(),
			inventory -> {
				assertEquals(inventory.getText(), request.getText());
				assertEquals(inventory.getComment(), request.getComment().orElse(null));
				assertEquals(inventory.getExternalId(), request.getExternalId());
				assertEquals(inventory.getOwner(), user);
				assertNotNull(inventory.getCreationDateTime());
				verify(repository, times(1)).save(inventory);
			}
		);
	}

	@Test
	public void createChildInventory() {
		// Given
		AppUser user = mock(AppUser.class);
		Inventory parent = mock(Inventory.class);
		String parentExternalId = "ext_1";
		CreateInventoryRequest request = new CreateInventoryRequest("ext_0", parentExternalId, 0, "some text", null, false);
		// When
		when(parent.getOwner()).thenReturn(user);
		when(parent.getExternalId()).thenReturn(parentExternalId);
		when(repository.findIdByOwnerAndExternalId(user, request.getExternalId())).thenReturn(null);
		when(repository.findByOwnerAndExternalId(user, parentExternalId)).thenReturn(parent);
		returnIdentityWhenSaveToRepository(repository, Inventory.class);
		whenTransactionThenExecute();
		// Then
		inventories.create(user, request).bimap(
			error -> alwaysFail(),
			item -> {
				assertEquals(item.getText(), request.getText());
				assertEquals(item.getParent(), parent);
				assertEquals(item.getComment(), request.getComment().orElse(null));
			}
		);
		verify(repository, times(1)).save(any(Inventory.class));
	}

	@Test
	public void findChildrenForInventory() {
		// Given
		AppUser user = mock(AppUser.class);
		Long parentId = 1L;
		String parentExternalId = "ext_1";
		Inventory child1 = mock(Inventory.class);
		Inventory child2 = mock(Inventory.class);
		// When
		when(repository.findIdByOwnerAndExternalId(user, parentExternalId)).thenReturn(parentId);
		when(repository.findAllChildren(user, parentId, null)).thenReturn(new PageImpl<>(asList(child1, child2)));
		// Then
		Page<Inventory> items = inventories.findAllChildren(user, parentExternalId, null);
		assertEquals(items.getContent(), asList(child1, child2));
	}

	@Test
	public void findInventoriesIsRestrictedToNonOwners() {
		// Given
		AppUser notTheOwner = mock(AppUser.class);
		String parentExternalId = "ext_1";
		// When
		when(repository.findIdByOwnerAndExternalId(notTheOwner, parentExternalId)).thenReturn(null);
		// Then
		Page<Inventory> childrenFromInventory = inventories.findAllChildren(notTheOwner, parentExternalId, null);
		assertFalse(childrenFromInventory.hasContent());
	}

	@Test
	public void synchronizeInventoriesTest() {
		// Given
		AppUser user = mock(AppUser.class);
		Inventory parent = mock(Inventory.class);
		String parentExternalId = "ext_1";
		SyncRequest createOne = SyncRequest.createInventoryRequest(new CreateInventoryRequest("ext_1_1", parentExternalId, 0, "create", null, false));
		SyncRequest deleteOne = SyncRequest.deleteInventoryRequest("ext_2");
		SyncRequest updateOne = SyncRequest.updateInventoryRequest("ext_3", new CreateInventoryRequest("ext_31", parentExternalId, 2, "update", null, false));
		Inventory deleteThis = mock(Inventory.class);
		Inventory updateThis = mock(Inventory.class);
		// When
		// When Create
		when(repository.findByOwnerAndExternalId(user, parentExternalId)).thenReturn(parent);
		doReturn(Either.right(mock(Inventory.class))).when(inventories).create(user, parent, createOne.getCreateInventory());
		// When Delete
		when(repository.findIdByOwnerAndExternalId(user, deleteOne.getExternalId())).thenReturn(2L);
		doReturn(Either.right(deleteThis)).when(inventories).softDeleteOne(eq(user), eq(deleteOne.getExternalId()), any());
		// When Update
		when(repository.findByOwnerAndExternalId(user, updateOne.getExternalId())).thenReturn(updateThis);
		doReturn(Either.right(updateOne)).when(inventories).updateOne(user, updateOne.getCreateInventory());
		// Then
		List<Either<InventoryService.InventoryError, Inventory>> results = inventories.synchronize(
			user, parentExternalId, asList(createOne, deleteOne, updateOne)
		);
		assertEquals(results.size(), 3);
		// Create check
		verify(inventories, times(1)).create(user, parent, createOne.getCreateInventory());
		// Delete check
		verify(inventories, times(1)).softDeleteOne(eq(user), eq(deleteOne.getExternalId()), any());
		// Update check
		verify(inventories, times(1)).updateOne(user, updateOne.getCreateInventory());
	}

	@Test
	public void inventoryUpdateTest() {
		// Given
		AppUser user = mock(AppUser.class);
		long id = 2L;
		String externalId = "ext_2";
		long existentParentId = 1L;
		String existentParentExternalId = "ext_1";
		CreateInventoryRequest request = new CreateInventoryRequest(externalId, existentParentExternalId, 5, "text", "comment", false);
		Inventory existent = mock(Inventory.class);
		// When
		when(repository.findByOwnerAndExternalId(user, externalId)).thenReturn(existent);
		when(existent.getParentId()).thenReturn(existentParentId);
		when(repository.findIdByOwnerAndExternalId(user, request.getParentExternalId().orElse(null))).thenReturn(existentParentId);
		// Then
		Either<InventoryService.InventoryError, Inventory> result = inventories.updateOne(user, request);
		assertTrue(result.isRight());
		verify(existent, times(1)).setChildIndex(request.getIndex());
		verify(existent, times(1)).setCompleted(request.isCompleted());
		verify(existent, times(1)).setComment(request.getComment().orElse(null));
		verify(existent, times(0)).setParent(any());
		verify(existent, times(1)).setText(request.getText());
		verify(repository, times(1)).save(existent);
	}

	@Test
	public void changeInventoryParentTest() {
		// Given
		AppUser user = mock(AppUser.class);
		long id = 3L;
		String externalId = "ext_3";
		long existentParentId = 1L;
		long newParentId = 2L;
		String newParentExternalId = "ext_2";
		CreateInventoryRequest request = new CreateInventoryRequest(externalId, newParentExternalId, 0, "text", null, false);
		Inventory existent = mock(Inventory.class);
		Inventory newParent = mock(Inventory.class);
		// When
		when(existent.getParentId()).thenReturn(existentParentId);
		when(repository.findByOwnerAndExternalId(user, externalId)).thenReturn(existent);
		when(repository.findByOwnerAndExternalId(user, newParentExternalId)).thenReturn(newParent);
		when(repository.save(existent)).thenReturn(mock(Inventory.class));
		// Then
		Either<InventoryService.InventoryError, Inventory> result = inventories.updateOne(user, request);
		assertTrue(result.isRight());
		verify(existent, times(1)).setChildIndex(request.getIndex());
		verify(existent, times(1)).setCompleted(request.isCompleted());
		verify(existent, times(1)).setComment(request.getComment().orElse(null));
		verify(existent, times(1)).setParent(newParent);
		verify(existent, times(1)).setText(request.getText());
		verify(repository, times(1)).save(existent);
	}

	private void whenTransactionThenExecute() {
		when(transactions.execute(any())).thenAnswer(invocation ->
			invocation.getArgumentAt(0, TransactionCallback.class).doInTransaction(null)
		);
	}
}
