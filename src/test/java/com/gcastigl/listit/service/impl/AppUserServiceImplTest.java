package com.gcastigl.listit.service.impl;

import com.gcastigl.listit.model.entity.AppUser;
import com.gcastigl.listit.model.io.CreateUserRequest;
import com.gcastigl.listit.repository.AppUserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import static com.gcastigl.listit.MockitoUtils.alwaysFail;
import static com.gcastigl.listit.MockitoUtils.returnIdentityWhenSaveToRepository;
import static com.gcastigl.listit.service.api.AppUserService.AppUserError.NICKNAME_EXISTS;
import static com.gcastigl.listit.service.api.AppUserService.AppUserError.NOT_AUTHORIZED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AppUserServiceImplTest {

	@Mock
	private AppUserRepository repository;

	@Mock
	private PasswordEncoder encoder;

	@Spy
	@InjectMocks
	private AppUserServiceImpl appUsers;

	@Test
	public void createRegularAppUser() {
		// Given
		CreateUserRequest request = new CreateUserRequest();
		request.setNickname("test");
		request.setPassword("1234");
		request.setAuthType(AppUser.AuthenticationType.USER_AND_PASS);
		request.setProfileType(AppUser.ProfileType.REGULAR);
		String encodedPassword = "some-password-hash";
		// When
		when(encoder.encode(request.getPassword())).thenReturn(encodedPassword);
		returnIdentityWhenSaveToRepository(repository, AppUser.class);
		// Then
		appUsers.create(null, request).bimap(
			error -> alwaysFail(),
			user -> {
				assertEquals(user.getPassword(), encodedPassword);
				assertEquals(user.getNickname(), request.getNickname());
				assertEquals(user.getAuthType(), request.getAuthType());
				assertEquals(user.getProfileType(), request.getProfileType());
				assertNotNull(user.getCreationDate());
			}
		);
	}

	@Test
	public void createPremiumAppUserIsRestrictedToAnonymousUsers() {
		// Given
		CreateUserRequest request = new CreateUserRequest();
		request.setNickname("test");
		request.setPassword("1234");
		request.setAuthType(AppUser.AuthenticationType.USER_AND_PASS);
		request.setProfileType(AppUser.ProfileType.PREMIUM);
		String encodedPassword = "some-password-hash";
		// When
		when(encoder.encode(request.getPassword())).thenReturn(encodedPassword);
		// Then
		appUsers.create(null, request).bimap(
			error -> {assertEquals(error, NOT_AUTHORIZED);},
			user -> alwaysFail()
		);
	}

	@Test
	public void restrictCreateUserIfNicknameExists() {
		// Given
		AppUser existingUser = mock(AppUser.class);
		CreateUserRequest request = new CreateUserRequest();
		request.setNickname("test");
		request.setPassword("1234");
		request.setAuthType(AppUser.AuthenticationType.USER_AND_PASS);
		request.setProfileType(AppUser.ProfileType.REGULAR);
		// When
		when(repository.findByNickname(request.getNickname())).thenReturn(existingUser);
		// Then
		appUsers.create(null, request).bimap(
			error -> {assertEquals(error, NICKNAME_EXISTS);},
			user -> alwaysFail()
		);
	}
}
