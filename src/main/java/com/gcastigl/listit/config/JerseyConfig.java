package com.gcastigl.listit.config;

import com.gcastigl.listit.web.AppUserResource;
import com.gcastigl.listit.web.AuthenticationFilter;
import com.gcastigl.listit.web.AuthenticationResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.gcastigl.listit.web.GQLResource;

@Configuration
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		register(AuthenticationFilter.class);
		register(AuthenticationResource.class);
		register(GQLResource.class);
		register(AppUserResource.class);
	}
}
