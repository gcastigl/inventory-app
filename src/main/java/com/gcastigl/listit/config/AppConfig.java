package com.gcastigl.listit.config;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@ComponentScan(basePackages = { "com.gcastigl.listit" })
@EnableJpaRepositories(basePackages = "com.gcastigl.listit.repository")
public class AppConfig {

	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.addConverter(new Converter<LocalDateTime, Date>() {
			@Override
			public Date convert(MappingContext<LocalDateTime, Date> context) {
				return Date.from(context.getSource().atZone(ZoneId.systemDefault()).toInstant());
			}
		});
		return modelMapper;
	}
	
	@Bean(name = "passwordEncoder")
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

}
