package com.gcastigl.listit.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gcastigl.listit.model.entity.AppUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

	AppUser findByNickname(String nickname);

	@Query("SELECT user.password FROM AppUser user WHERE user.nickname = :nickname")
	String getPassword(@Param("nickname") String nickname);

}
