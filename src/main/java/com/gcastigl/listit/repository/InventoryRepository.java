package com.gcastigl.listit.repository;

import com.gcastigl.listit.model.entity.AppUser;
import com.gcastigl.listit.model.entity.Inventory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface InventoryRepository extends JpaRepository<Inventory, Long> {

	@Query("SELECT i FROM Inventory i WHERE i.owner = :owner AND i.deleted = FALSE AND i.externalId = :externalId")
	Inventory findByOwnerAndExternalId(@Param("owner") AppUser owner, @Param("externalId") String externalId);

	@Query("SELECT i.id FROM Inventory i WHERE i.owner = :owner AND i.deleted = FALSE AND i.externalId = :externalId")
	Long findIdByOwnerAndExternalId(@Param("owner") AppUser owner, @Param("externalId") String externalId);

	@Query(
		" SELECT i FROM Inventory i " +
		" WHERE i.owner = :owner " +
		"   AND i.deleted = FALSE AND i.childIndex = :index" +
		"   AND ((:parentId IS NULL AND i.parent IS NULL) OR (i.parent.id = :parentId))"
	)
	Inventory findByOwnerAndIndex(
		@Param("owner") AppUser owner, @Param("parentId") Long parentId, @Param("index") int index
	);

	@Modifying
	@Query(
		"UPDATE Inventory SET childIndex = childIndex + 1 " +
		" WHERE childIndex >= :index " +
		" AND ((:parentId IS NULL AND parent IS NULL) OR (parent.id = :parentId)) " +
		" AND owner = :owner " +
		" AND deleted = FALSE "
	)
	void updateAllIndexesByOne(
		@Param("owner") AppUser owner, @Param("parentId") Long parentId, @Param("index") int index
	);

	@Query(
		"SELECT inventory FROM Inventory inventory " +
		" WHERE inventory.owner = :owner " +
		" AND ((:parentId IS NULL AND inventory.parent IS NULL) OR (inventory.parent.id = :parentId)) " +
		" AND inventory.deleted = FALSE" +
		" ORDER BY inventory.childIndex"
	)
	Page<Inventory> findAllChildren(
		@Param("owner") AppUser owner, @Param("parentId") Long parentId, Pageable pageable
	);


	@Query(
		"SELECT COUNT(*) FROM Inventory inventory " +
		" WHERE inventory.owner = :owner " +
		" AND ((:parentExternalId IS NULL AND inventory.parent IS NULL) OR (inventory.parent.externalId = :parentExternalId))" +
		" AND inventory.deleted = FALSE"
	)
	Long getChildrenCount(@Param("owner") AppUser owner, @Param("parentExternalId") String parentExternalId);

	@Query(
		"SELECT inventory.externalId FROM Inventory inventory " +
		" WHERE inventory.owner = :owner " +
		" AND ((:parentExternalId IS NULL AND inventory.parent IS NULL) OR (inventory.parent.externalId = :parentExternalId))" +
		" AND inventory.deleted = FALSE"
	)
	List<String> findIdsByOwnerAndParentId(@Param("owner") AppUser owner, @Param("parentExternalId") String parentExternalId);

	@Modifying
	@Query("DELETE FROM Inventory inventory WHERE inventory.externalId IN :externalIds")
	void deleteAllById(@Param("externalIds") List<String> externalIds);

	@Query("SELECT i.externalId FROM Inventory i WHERE i.id = :id")
	String findExternalIdById(@Param("id") long id);

	@Modifying
	@Query("UPDATE Inventory SET child_index = :index WHERE owner = :owner AND parent.id = :parentId AND externalId = :externalId")
	void updateIndex(@Param("owner") AppUser user, @Param("parentId") long parentId,
	                 @Param("externalId") String externalId, @Param("index") int index);

	@Query(
		"SELECT inventory FROM Inventory inventory " +
		" WHERE inventory.deleted = TRUE" +
		" AND DATE(inventory.updateDateTime) <= :maxUpdateDate"
	)
	Page<Inventory> findAllSoftDeleted(@Param("maxUpdateDate") LocalDate maxUpdateDate, Pageable pageable);

}
