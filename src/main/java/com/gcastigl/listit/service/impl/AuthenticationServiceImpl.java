package com.gcastigl.listit.service.impl;


import com.gcastigl.listit.model.entity.AppUser;
import com.gcastigl.listit.model.entity.AppUser.AuthenticationType;
import com.gcastigl.listit.model.entity.AppUser.ProfileType;
import com.gcastigl.listit.model.io.AuthenticationRequest;
import com.gcastigl.listit.model.io.AuthenticationToken;
import com.gcastigl.listit.model.io.CreateUserRequest;
import com.gcastigl.listit.service.api.AppUserService;
import com.gcastigl.listit.service.api.AuthenticationService;
import com.gcastigl.listit.service.api.OAuthService;
import com.gcastigl.listit.util.Either;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static com.gcastigl.listit.model.entity.AppUser.AuthenticationType.FACEBOOK;
import static com.gcastigl.listit.model.entity.AppUser.AuthenticationType.USER_AND_PASS;
import static com.gcastigl.listit.service.api.AuthenticationService.AuthenticationErrorType.INVALID_TOKEN;
import static com.google.common.collect.Iterables.getOnlyElement;
import static java.util.Collections.singletonList;

@Service
class AuthenticationServiceImpl implements AuthenticationService {

	@Autowired
	private AppUserService users;

	@Value("${token.secret}")
	private String secret_;

	@Value("${token.hoursToExpiration}")
	private Long hoursToExpiration_;

	@Autowired
	private PasswordEncoder encoder;

	@Autowired
	private OAuthService oAuthServices;

	@Autowired
	private SpringTransactionService transactions;

	private final String SEPARATOR = ":";

	private final AccountStatusUserDetailsChecker accountChecker = new AccountStatusUserDetailsChecker();

	private UserDetails loadUserByUsername(String username, AuthenticationType authType) throws UsernameNotFoundException {
		return users.findByNickname(username, authType).map(this::buildUserDetails)
			.orElseThrow(() -> new UsernameNotFoundException(username));
	}

	@Override
	public UserDetails buildUserDetails(AppUser user) {
		String username = user.getNickname();
		String password = user.getPassword();
		boolean enabled = user.isEnabled();
		ProfileType profileType = user.getProfileType();
		List<GrantedAuthority> authorities = singletonList(new SimpleGrantedAuthority(profileType.name()));
		return new User(username, password, enabled, true, true, true, authorities);
	}

	@Override
	public Either<AuthenticationErrorType, AuthenticationToken> authenticate(AuthenticationRequest request) {
		if (USER_AND_PASS.equals(request.getType())) {
			return Either.right(createToken(request.getUsername(), request.getPassword(), request.getType(), Optional.empty()));
		} else if (FACEBOOK.equals(request.getType())) {
			String fbToken = request.getPassword();
			Optional<Pair<String, String>> nicknameAndName = oAuthServices.getNicknameAndNameFromFacebookToken(fbToken);
			if (!nicknameAndName.isPresent()) {
				return Either.left(INVALID_TOKEN);
			} else {
				AppUser user = createUserFromOAuth(nicknameAndName.get(), fbToken, AuthenticationType.FACEBOOK);
				return Either.right(createToken(user, Optional.empty()));
			}
		}
		return Either.left(INVALID_TOKEN);
	}

	private AppUser createUserFromOAuth(Pair<String, String> nicknameAndName, String password, AuthenticationType authenticationType) {
		String nickname = nicknameAndName.getLeft();
		Optional<AppUser> user = users.findByNickname(nickname, authenticationType);
		if (!user.isPresent()) {
			CreateUserRequest createUser = new CreateUserRequest();
			createUser.setNickname(nickname);
			createUser.setPassword(password);
			createUser.setProfileType(ProfileType.REGULAR);
			createUser.setAuthType(authenticationType);
			createUser.setDisplayName(nicknameAndName.getRight());
			return transactions.execute(transaction -> users.create(null, createUser))
				.either(error -> null, success -> success);
		} else {
			users.setRawPassword(user.get(), password);
			return transactions.execute(transaction -> users.save(user.get()));
		}
	}

	@Override
	public AuthenticationToken createToken(String username, String rawPassword, AuthenticationType authType, Optional<Long> hoursToExpiration) {
		Optional<AppUser> user = users.findByNickname(username, authType);
		if (!user.isPresent()) {
			throw new UsernameNotFoundException(username);
		}
		UserDetails userdetails = validateUserDetails(buildUserDetails(user.get()), rawPassword);
		Long expiration = expiration(hoursToExpiration);
		String token = generateToken(username, userdetails.getPassword(), expiration, authType);
		String authority = getOnlyElement(userdetails.getAuthorities()).getAuthority();
		return new AuthenticationToken(username, user.get().getDisplayName(), expiration, ProfileType.valueOf(authority), token);
	}

	@Override
	public AuthenticationToken createToken(AppUser user, Optional<Long> hoursToExpiration) {
		Long expiration = expiration(hoursToExpiration);
		String token = generateToken(user.getNickname(), user.getPassword(), expiration, user.getAuthType());
		return new AuthenticationToken(user.getNickname(), user.getDisplayName(), expiration, user.getProfileType(), token);
	}

	private Long expiration(Optional<Long> hoursToExpiration) {
		return System.currentTimeMillis() + TimeUnit.HOURS.toMillis(hoursToExpiration.orElse(hoursToExpiration_));
	}

	private UserDetails validateUserDetails(UserDetails user, String rawPassword) {
		accountChecker.check(user);
		if (!encoder.matches(rawPassword, user.getPassword())) {
			throw new BadCredentialsException("Invalid password");
		}
		return user;
	}

	String generateToken(String username, String password, Long expiration, AuthenticationType authenticationType) {
		StringBuilder token = new StringBuilder();
		token.append(username).append(SEPARATOR);
		token.append(expiration).append(SEPARATOR);
		token.append(authenticationType.ordinal()).append(SEPARATOR);
		token.append(encoder.encode(genRawSignature(username, password, expiration, authenticationType)));
		return token.toString();
	}

	String genRawSignature(String username, String password, Long expiration, AuthenticationType authenticationType) {
		StringBuilder signature = new StringBuilder();
		signature.append(username).append(SEPARATOR);
		signature.append(password).append(SEPARATOR);
		signature.append(expiration).append(SEPARATOR);
		signature.append(authenticationType.ordinal()).append(SEPARATOR);
		signature.append(secret_);
		return signature.toString();
	}

	@Override
	public Optional<AppUser> getUserFromToken(String token) {
		if (!isValidToken(token)) {
			return Optional.empty();
		}
		String[] parts = token.split(SEPARATOR);
		return users.findByNickname(parts[0], AuthenticationType.values()[Integer.valueOf(parts[2])]);
	}

	private boolean isValidToken(String token) {
		if (token == null) {
			return false;
		}
		String[] parts = token.split(SEPARATOR);
		if (parts.length != 4) {
			return false;
		}
		try {
			String username = parts[0];
			Long expiration = Long.parseLong(parts[1]);
			AuthenticationType authType = AuthenticationType.values()[Integer.valueOf(parts[2])];
			String signature = parts[3];
			if (expiration <= System.currentTimeMillis()) {
				return false;
			}
			String password = users.getEncodedPassword(username);
			return encoder.matches(genRawSignature(username, password, expiration, authType), signature);
		} catch (Exception e) {
			return false;
		}
	}

}