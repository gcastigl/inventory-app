package com.gcastigl.listit.service.impl;

import com.gcastigl.listit.model.entity.AppUser;
import com.gcastigl.listit.model.io.CreateUserRequest;
import com.gcastigl.listit.repository.AppUserRepository;
import com.gcastigl.listit.service.api.AppUserService;
import com.gcastigl.listit.util.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

import static com.gcastigl.listit.service.api.AppUserService.AppUserError.EMPTY_REQUEST;
import static com.gcastigl.listit.service.api.AppUserService.AppUserError.MISSING_PARAMETERS;
import static com.gcastigl.listit.service.api.AppUserService.AppUserError.NICKNAME_EXISTS;
import static com.gcastigl.listit.service.api.AppUserService.AppUserError.NOT_AUTHORIZED;
import static com.google.common.base.Strings.emptyToNull;
import static com.google.common.base.Strings.nullToEmpty;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

@Service
public class AppUserServiceImpl implements AppUserService {

	@Autowired
	private AppUserRepository repository;

	@Autowired
	private PasswordEncoder encoder;

	@Override
	public Either<AppUserError, AppUser> create(AppUser user, CreateUserRequest request) {
		if (isNull(request)) {
			return Either.left(EMPTY_REQUEST);
		}
		if (isNull(request.getNickname()) || isNull(request.getPassword()) || isNull(request.getProfileType()) || isNull(request.getAuthType())) {
			return Either.left(MISSING_PARAMETERS);
		}
		if (AppUser.ProfileType.PREMIUM.equals(request.getProfileType())) {
			return Either.left(NOT_AUTHORIZED);
		}
		if (repository.findByNickname(request.getNickname()) != null) {
			return Either.left(NICKNAME_EXISTS);
		}
		String encodedPassword = encoder.encode(request.getPassword());
		AppUser newUser = new AppUser(
			request.getProfileType(), request.getNickname(), encodedPassword, LocalDateTime.now(), request.getAuthType()
		);
		newUser.setDisplayName(buildDisplayName(request));
		return Either.right(repository.save(newUser));
	}

	private String buildDisplayName(CreateUserRequest request) {
		String displayName = nullToEmpty(request.getDisplayName()).trim();
		if (displayName.isEmpty()) {
			String nickname = nullToEmpty(request.getNickname()).trim();
			int atIndex = nickname.indexOf("@");
			if (atIndex != -1) {
				nickname = nickname.substring(0, atIndex);
			}
			displayName = nickname;
		}
		return emptyToNull(displayName);
	}

	@Override
	public void setRawPassword(AppUser user, String rawPassword) {
		user.setPassword(encoder.encode(rawPassword));
	}

	@Override
	public Optional<AppUser> findByNickname(String nickname, AppUser.AuthenticationType authenticationType) {
		return Optional.ofNullable(repository.findByNickname(requireNonNull(nickname)));
	}

	@Override
	public String getEncodedPassword(String nickname) {
		return repository.getPassword(nickname);
	}

	@Override
	public AppUser save(AppUser user) {
		return repository.save(user);
	}
}
