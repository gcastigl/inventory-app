package com.gcastigl.listit.service.impl;

import com.gcastigl.listit.service.api.OAuthService;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
class OAuthServiceImpl implements OAuthService {

	private final RestTemplate restTemplate = new RestTemplate();


	@Override
	public Optional<Pair<String, String>> getNicknameAndNameFromFacebookToken(String token) {
		final String meUrl = "https://graph.facebook.com/me?access_token={token}";
		FacebookMeResponse response = restTemplate.getForObject(meUrl, FacebookMeResponse.class, token);
		if (response.getError() != null) {
			return Optional.empty();
		}
		return Optional.of(Pair.of(response.getId(), response.getName()));
	}

	private static class FacebookMeResponse {
		String name;
		String id;
		FacebookMeError error;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public FacebookMeError getError() {
			return error;
		}

		public void setError(FacebookMeError error) {
			this.error = error;
		}
	}

	private static class FacebookMeError {
		String message;
		String type;
		int code;
		String fbtrace_id;

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public int getCode() {
			return code;
		}

		public void setCode(int code) {
			this.code = code;
		}

		public String getFbtrace_id() {
			return fbtrace_id;
		}

		public void setFbtrace_id(String fbtrace_id) {
			this.fbtrace_id = fbtrace_id;
		}
	}
}
