package com.gcastigl.listit.service.impl;

import com.gcastigl.listit.model.entity.AppUser;
import com.gcastigl.listit.model.entity.Inventory;
import com.gcastigl.listit.model.io.CreateInventoryRequest;
import com.gcastigl.listit.model.io.SyncRequest;
import com.gcastigl.listit.repository.InventoryRepository;
import com.gcastigl.listit.service.api.AppUserService;
import com.gcastigl.listit.service.api.InventoryService;
import com.gcastigl.listit.util.Either;
import com.gcastigl.listit.util.Pages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.gcastigl.listit.service.api.InventoryService.InventoryError.EXTERNAL_ID_EXISTS;
import static com.gcastigl.listit.service.api.InventoryService.InventoryError.INVALID_COMMENT;
import static com.gcastigl.listit.service.api.InventoryService.InventoryError.INVALID_DATE;
import static com.gcastigl.listit.service.api.InventoryService.InventoryError.INVALID_EXTERNAL_ID;
import static com.gcastigl.listit.service.api.InventoryService.InventoryError.INVALID_ID;
import static com.gcastigl.listit.service.api.InventoryService.InventoryError.INVALID_PARAMETERS;
import static com.gcastigl.listit.service.api.InventoryService.InventoryError.INVALID_PARENT;
import static com.gcastigl.listit.service.api.InventoryService.InventoryError.INVALID_TEXT;
import static com.gcastigl.listit.service.api.InventoryService.InventoryError.INVALID_TIME;
import static com.gcastigl.listit.service.api.InventoryService.InventoryError.NOT_OWNER;
import static com.gcastigl.listit.service.api.InventoryService.InventoryError.NULL_OWNER;
import static com.gcastigl.listit.service.api.InventoryService.InventoryError.NULL_VALUE;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.base.Strings.nullToEmpty;
import static java.util.Collections.nCopies;
import static java.util.Collections.singletonList;
import static java.util.Objects.isNull;

@Service
class InventoryServiceImpl implements InventoryService {


	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");

	private static Either<InventoryError, Optional<LocalDate>> buildDueDate(String dueDate) {
		try {
			return Either.right(
				isNullOrEmpty(dueDate) ? Optional.empty() : Optional.of(LocalDate.parse(dueDate, DATE_FORMATTER))
			);
		} catch (DateTimeParseException e) {
			return Either.left(INVALID_DATE);
		}
	}

	private static Either<InventoryError, Optional<LocalTime>> buildDueTime(String dueTime) {
		try {
			return Either.right(
				isNullOrEmpty(dueTime) ? Optional.empty() : Optional.of(LocalTime.parse(dueTime, TIME_FORMATTER))
			);
		} catch (DateTimeParseException e) {
			return Either.left(INVALID_TIME);
		}
	}

	private static String cleanup(String s) {
		return nullToEmpty(s).trim();
	}

	@Autowired
	private InventoryRepository repository;

	@Autowired
	private AppUserService users;

	@Autowired
	private SpringTransactionService transactions;

	@Override
	public Either<InventoryError, Inventory> create(AppUser user, CreateInventoryRequest request) {
		Optional<Inventory> parent = Optional.empty();
		Optional<String> parentExternalId = request.getParentExternalId();
		if (parentExternalId.isPresent()) {
			parent = findOne(user, parentExternalId.get());
			if (!parent.isPresent()) {
				return Either.left(INVALID_PARENT);
			}
		}
		return create(user, parent.orElse(null), request);
	}

	Either<InventoryError, Inventory> create(AppUser user, Inventory parent, CreateInventoryRequest request) {
		Optional<InventoryError> error = validateFields(user, request);
		if (error.isPresent()) {
			return Either.left(error.get());
		}
		if (repository.findIdByOwnerAndExternalId(user, request.getExternalId()) != null) {
			return Either.left(EXTERNAL_ID_EXISTS);
		}
		return transactions.execute(transaction -> {
			Long parentId = parent == null ? null : parent.getId();
			int index = request.getIndex();
			if (repository.findByOwnerAndIndex(user, parentId, index) != null) {
				repository.updateAllIndexesByOne(user, parentId, request.getIndex());
			}
			String text = request.getText();
			String comment = request.getComment().orElse(null);
			boolean completed = request.isCompleted();
			String externalId = request.getExternalId();
			LocalDateTime creationDateTime = Optional.ofNullable(request.getCreationDateTime()).orElse(LocalDateTime.now());
			Inventory inventory = new Inventory(user, parent, index, text, comment, completed, externalId, creationDateTime);
			inventory.setDueDate(buildDueDate(request.getDueDate()).either(none -> null, maybeDate -> maybeDate.orElse(null)));
			inventory.setDueTime(buildDueTime(request.getDueTime()).either(none -> null, maybeDate -> maybeDate.orElse(null)));
			inventory.setReminderType(request.getReminderType());
			return Either.right(repository.save(inventory));
		});
	}

	// TODO: rename this function to buildInventory and make it return a Either<InventoryError, Inventory>
	private Optional<InventoryError> validateFields(AppUser user, CreateInventoryRequest request) {
		if (isNull(user)) {
			return Optional.of(NULL_OWNER);
		}
		if (isNull(request)) {
			return Optional.of(NULL_VALUE);
		}
		String externalId = cleanup(request.getExternalId());
		if (externalId.isEmpty() || externalId.length() > Inventory.EXTERNAL_ID_LEN) {
			return Optional.of(INVALID_EXTERNAL_ID);
		}
		request.setExternalId(externalId);
		String parentExternalId = cleanup(request.getParentExternalId().orElse(null));
		if (parentExternalId.length() > Inventory.EXTERNAL_ID_LEN) {
			return Optional.of(INVALID_EXTERNAL_ID);
		}
		if (parentExternalId.equals(externalId)) {
			return Optional.of(INVALID_EXTERNAL_ID);
		}
		request.setParentExternalId(parentExternalId);
		String text = cleanup(request.getText());
		if (text.isEmpty() || text.length() > Inventory.TEXT_MAX_LEN) {
			return Optional.of(INVALID_TEXT);
		}
		request.setText(text);
		String comment = cleanup(request.getComment().orElse(null));
		if (comment.length() > Inventory.COMMENT_MAX_LEN) {
			return Optional.of(INVALID_COMMENT);
		}
		request.setComment(comment);
		Optional<InventoryError> dueDateError = buildDueDate(request.getDueDate()).either(Optional::of, ok -> Optional.empty());
		if (dueDateError.isPresent()) {
			return dueDateError;
		}
		Optional<InventoryError> dueTimeError = buildDueTime(request.getDueTime()).either(Optional::of, ok -> Optional.empty());
		if (dueTimeError.isPresent()) {
			return dueTimeError;
		}
		return Optional.empty();
	}

	@Override
	public Optional<Inventory> findOne(AppUser user, String externalId) {
		return Optional.ofNullable(repository.findByOwnerAndExternalId(user, externalId));
	}

	@Override
	public Page<Inventory> findAllChildren(AppUser user, String parentExternalId, Pageable pageable) {
		Long parentId = null;
		if (!isNull(parentExternalId)) {
			parentId = repository.findIdByOwnerAndExternalId(user, parentExternalId);
			if (isNull(parentId)) {
				return Pages.emptyPage(pageable);
			}
		}
		return repository.findAllChildren(user, parentId, pageable);
	}

	@Override
	@Transactional
	public Either<InventoryError, Inventory> updateOne(AppUser user, CreateInventoryRequest request) {
		Optional<InventoryError> error = validateFields(user, request);
		if (error.isPresent()) {
			return Either.left(error.get());
		}
		Optional<Inventory> inventory = findOne(user, request.getExternalId());
		if (!inventory.isPresent()) {
			return Either.left(NOT_OWNER);
		}
		Long currentParentId = inventory.get().getParentId();
		String newParentExternalId = request.getParentExternalId().orElse(null);
		Long newParentId = null;
		if (!isNull(newParentExternalId)) {
			newParentId = repository.findIdByOwnerAndExternalId(user, newParentExternalId);
			if (isNull(newParentId)) {
				return Either.left(INVALID_PARENT);
			}
		}
		boolean updateParent = !Objects.equals(currentParentId, newParentId);
		Optional<Inventory> parent = updateParent && !isNull(newParentId) ? findOne(user, newParentExternalId) : Optional.empty();
		if (updateParent && !isNull(newParentId) && !parent.isPresent()) {
			return Either.left(NOT_OWNER);
		}
		inventory.ifPresent(present -> {
			present.setText(request.getText());
			present.setComment(request.getComment().orElse(null));
			if (updateParent) {
				present.setParent(parent.orElse(null));
			}
			present.setCompleted(request.isCompleted());
			present.setChildIndex(request.getIndex());
			present.setUpdateDateTime(LocalDateTime.now());
			present.setDueDate(buildDueDate(request.getDueDate()).either(none -> null, maybeDate -> maybeDate.orElse(null)));
			present.setDueTime(buildDueTime(request.getDueTime()).either(none -> null, maybeDate -> maybeDate.orElse(null)));
			present.setReminderType(request.getReminderType());
			repository.save(present);
		});
		return Either.right(inventory.get());
	}

	@Override
	public long getChildrenCount(AppUser user, String parentExternalId) {
		Long count = repository.getChildrenCount(user, parentExternalId);
		return isNull(count) ? 0 : count;
	}

	@Override
	public List<String> findAllChildrenRecursively(AppUser user, String parentExternalId) {
		List<String> allChildrenId = new LinkedList<>();
		allChildrenId.add(parentExternalId);
		for (String childExternalId : repository.findIdsByOwnerAndParentId(user, parentExternalId)) {
			allChildrenId.addAll(findAllChildrenRecursively(user, childExternalId));
		}
		return allChildrenId;
	}

	@Override
	@Transactional
	public List<String> deleteOne(Inventory inventory) {
		// TODO: get children using inventory.id insteead of the external id!
		List<String> idsToDelete = findAllChildrenRecursively(inventory.getOwner(), inventory.getExternalId());
		repository.deleteAllById(idsToDelete);
		return idsToDelete;
	}

	@Override
	@Transactional
	public Either<InventoryError, Inventory> softDeleteOne(AppUser user, String externalId, LocalDateTime deletionDate) {
		Optional<Inventory> inventory = findOne(user, externalId);
		if (!inventory.isPresent()) {
			return Either.left(NOT_OWNER);
		}
		inventory.ifPresent(present -> {
			present.setDeleted(true);
			present.setUpdateDateTime(deletionDate == null ? LocalDateTime.now() : deletionDate);
			repository.save(present);
		});
		return Either.right(inventory.get());
	}

	@Override
	public List<Either<InventoryError, Inventory>> synchronize(AppUser user, String parentExternalId, List<SyncRequest> requests) {
		if (requests == null) {
			return singletonList(Either.left(NULL_VALUE));
		}
		int requestsSize = requests.size();
		if (user == null) {
			return nCopies(requestsSize, Either.left(NULL_OWNER));
		}
		Optional<Inventory> parent = Optional.empty();
		if (!isNull(parentExternalId)) {
			parent = findOne(user, parentExternalId);
			if (!parent.isPresent()) {
				return nCopies(requestsSize, Either.left(INVALID_PARENT));
			}
		}
		LocalDateTime now = LocalDateTime.now();
		List<Either<InventoryError, Inventory>> results = new ArrayList<>(requestsSize);
		for (SyncRequest request : requests) {
			String externalId = request.getExternalId();
			if (isNull(externalId)) {
				results.add(Either.left(INVALID_EXTERNAL_ID));
				continue;
			}
			if (request.isDelete()) {
				// Delete inventory
				boolean idExists = repository.findIdByOwnerAndExternalId(user, externalId) != null;
				results.add(idExists ? softDeleteOne(user, externalId, now) : Either.left(INVALID_ID));
			} else if (request.getCreateInventory() != null) {
				request.getCreateInventory().setCreationDateTime(now);
				Optional<Inventory> match = Optional.ofNullable(repository.findByOwnerAndExternalId(user, externalId));
				if (!match.isPresent()) {
					// Create inventory
					results.add(create(user, parent.orElse(null), request.getCreateInventory()));
				} else {
					// Update inventory
					results.add(updateOne(user, request.getCreateInventory()));
				}
			} else {
				results.add(Either.left(INVALID_PARAMETERS));
			}
		}
		return results;
	}

	@Override
	public Either<InventoryError, Inventory> updateChildrenIndexes(AppUser user, String parentExternalId, List<String> childrenIdOrder) {
		return transactions.execute(status -> {
			Optional<Inventory> parent = findOne(user, parentExternalId);
			parent.ifPresent(present -> {
				int i = 0;
				for (String childId : childrenIdOrder) {
					repository.updateIndex(user, present.getId(), childId, i++);
				}
			});
			return parent.map(Either::<InventoryError, Inventory> right).orElse(Either.left(INVALID_PARENT));
		});
	}

	@Override
	public String getExternalIdForId(Long id) {
		return isNull(id) ? null : repository.findExternalIdById(id);
	}

	@Override
	public Page<Inventory> findAllSoftDeleted(LocalDate maxUpdateDate, Pageable pageable) {
		return repository.findAllSoftDeleted(maxUpdateDate, pageable);
	}
}
