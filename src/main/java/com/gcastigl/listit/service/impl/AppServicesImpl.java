package com.gcastigl.listit.service.impl;

import com.gcastigl.listit.service.api.AuthenticationService;
import com.gcastigl.listit.service.api.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcastigl.listit.service.api.AppServices;
import com.gcastigl.listit.service.api.AppUserService;

@Service
public class AppServicesImpl implements AppServices {

	@Autowired
	private AuthenticationService authentication;

	@Autowired
	private AppUserService appusers;

	@Autowired
	private InventoryService inventories;

	@Override
	public AuthenticationService authentication() {
		return authentication;
	}

	@Override
	public AppUserService appusers() {
		return appusers;
	}

	@Override
	public InventoryService inventories() {
		return inventories;
	}

}
