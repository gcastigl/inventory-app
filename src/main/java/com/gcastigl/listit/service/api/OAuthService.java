package com.gcastigl.listit.service.api;

import org.apache.commons.lang3.tuple.Pair;

import java.util.Optional;

public interface OAuthService {

	Optional<Pair<String, String>> getNicknameAndNameFromFacebookToken(String token);

}
