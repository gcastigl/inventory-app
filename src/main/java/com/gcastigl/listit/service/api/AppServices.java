package com.gcastigl.listit.service.api;

public interface AppServices {

	AuthenticationService authentication();

	AppUserService appusers();

	InventoryService inventories();

}
