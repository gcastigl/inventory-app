package com.gcastigl.listit.service.api;

import java.util.Optional;

import com.gcastigl.listit.model.entity.AppUser;
import com.gcastigl.listit.model.entity.AppUser.AuthenticationType;
import com.gcastigl.listit.model.io.AuthenticationRequest;
import com.gcastigl.listit.model.io.AuthenticationToken;
import com.gcastigl.listit.util.Either;
import org.springframework.security.core.userdetails.UserDetails;

public interface AuthenticationService {

	enum AuthenticationErrorType {
		INVALID_CREDENTIALS,
		USER_DISABLED,
		INVALID_TOKEN
	}

	UserDetails buildUserDetails(AppUser user);

	Either<AuthenticationErrorType, AuthenticationToken> authenticate(AuthenticationRequest request);

	// TODO: make this return error type instead of throwing exceptions
	AuthenticationToken createToken(String username, String password, AuthenticationType authType, Optional<Long> hoursToExpiration);

	// TODO: make this return error type instead of throwing exceptions
	AuthenticationToken createToken(AppUser user, Optional<Long> hoursToExpiration);

	Optional<AppUser> getUserFromToken(String token);

}