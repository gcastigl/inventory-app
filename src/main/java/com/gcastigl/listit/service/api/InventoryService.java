package com.gcastigl.listit.service.api;

import com.gcastigl.listit.model.entity.AppUser;
import com.gcastigl.listit.model.entity.Inventory;
import com.gcastigl.listit.model.io.CreateInventoryRequest;
import com.gcastigl.listit.model.io.SyncRequest;
import com.gcastigl.listit.util.Either;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface InventoryService {

	enum InventoryError {
		NULL_VALUE, NULL_OWNER, INVALID_TEXT, INVALID_PARENT, NOT_OWNER,
		INVALID_COMMENT, INVALID_ID, INVALID_EXTERNAL_ID, EXTERNAL_ID_EXISTS,
		INVALID_PARAMETERS, INVALID_DATE, INVALID_TIME
	}

	Either<InventoryError, Inventory> create(AppUser user, CreateInventoryRequest request);

	Optional<Inventory> findOne(AppUser user, String externalId);

	Page<Inventory> findAllChildren(AppUser user, String parentExternalId, Pageable pageable);

	Either<InventoryError, Inventory> updateOne(AppUser user, CreateInventoryRequest request);

	long getChildrenCount(AppUser user, String parentId);

	List<String> findAllChildrenRecursively(AppUser user, String parentId);

	List<String> deleteOne(Inventory inventory);

	Either<InventoryError, Inventory> softDeleteOne(AppUser user, String externalId, LocalDateTime deletionDate);

	/**
	 * @param user current user. Must be the owner of the parentId inventory (if provided)
	 * @param parentExternalId parent inventory of the newInventories
	 *
	 *  For each request:
	 *    If Create => send createInventory
	 *    If Update index => send  id, childIndex
	 *    If Update values => send id, createInventory
	 *    If Delete => send id, delete
	 */
	List<Either<InventoryError, Inventory>> synchronize(AppUser user, String parentExternalId, List<SyncRequest> requests);

	Either<InventoryError, Inventory> updateChildrenIndexes(AppUser user, String parentExternalId, List<String> childrenIdOrder);

	String getExternalIdForId(Long id);

	Page<Inventory> findAllSoftDeleted(LocalDate maxUpdateDate, Pageable pageable);

}
