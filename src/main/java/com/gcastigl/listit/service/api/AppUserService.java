package com.gcastigl.listit.service.api;

import com.gcastigl.listit.model.entity.AppUser;
import com.gcastigl.listit.model.entity.AppUser.AuthenticationType;
import com.gcastigl.listit.model.io.CreateUserRequest;
import com.gcastigl.listit.util.Either;

import java.util.Optional;

public interface AppUserService {

	enum AppUserError {
		EMPTY_REQUEST,
		MISSING_PARAMETERS,
		NICKNAME_EXISTS,
		NOT_AUTHORIZED
	}

	Either<AppUserError, AppUser> create(AppUser user, CreateUserRequest request);

	Optional<AppUser> findByNickname(String nickname, AuthenticationType authenticationType);

	void setRawPassword(AppUser user, String rawPassword);

	String getEncodedPassword(String nickname);

	AppUser save(AppUser user);

}
