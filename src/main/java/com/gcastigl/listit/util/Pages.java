package com.gcastigl.listit.util;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static java.util.Collections.emptyList;

public class Pages {

	public static <T> Page<T> of(List<T> list) {
		return new PageImpl<>(list);
	}

	public static <T> Page<T> emptyPage() {
		return new PageImpl<>(emptyList());
	}

	public static <T> Page<T> emptyPage(Pageable pageable) {
		return new PageImpl<>(emptyList(), pageable, 0);
	}

}
