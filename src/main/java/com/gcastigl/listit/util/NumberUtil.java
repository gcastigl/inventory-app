package com.gcastigl.listit.util;

import java.util.Optional;

public class NumberUtil {

	public static Optional<Long> parseLong(String s) {
		try {
			return Optional.of(Long.parseLong(s));
		} catch (Exception e) {
			return Optional.empty();
		}
	}
}
