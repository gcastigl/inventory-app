package com.gcastigl.listit.util;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.function.Consumer;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

public abstract class Either<L, R> {

	public static <L, R> Either<L, R> left(L value) {
		return new Left<>(value);
	}

	public static <L, R> Either<L, R> right(R value) {
		return new Right<>(value);
	}

	private Either() {
	}

	public final <P> Either<P, R> first(Function<L, P> f) {
		return bimap(f, Function.identity());
	}

	public final <Q> Either<L, Q> second(Function<R, Q> f) {
		return bimap(Function.identity(), f);
	}

	public final boolean isRight() {
		return !isLeft();
	}

	public final boolean isLeft() {
		return this instanceof Left;
	}

	public abstract <C> C either(Function<L, C> fl, Function<R, C> fr);

	public abstract <P, Q> Either<P, Q> bimap(Function<L, P> fl, Function<R, Q> fr);

	public final Either<L, R> bimap(Consumer<L> cl, Consumer<R> cr) {
		return bimap(left -> {
			cl.accept(left);
			return left;
		}, right -> {
			cr.accept(right);
			return right;
		});
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	private static final class Left<L, R> extends Either<L, R> {

		private L value;

		Left(L value) {
			this.value = requireNonNull(value);
		}

		@Override
		public <P, Q> Either<P, Q> bimap(Function<L, P> fl, Function<R, Q> fr) {
			return new Left<>(fl.apply(value));
		}

		@Override
		public <C> C either(Function<L, C> fl, Function<R, C> fr) {
			return fl.apply(value);
		}
	}

	private static final class Right<L, R> extends Either<L, R> {

		private R value;

		Right(R value) {
			this.value = requireNonNull(value);
		}

		@Override
		public <P, Q> Either<P, Q> bimap(Function<L, P> fl, Function<R, Q> fr) {
			return new Right<>(fr.apply(value));
		}

		@Override
		public <C> C either(Function<L, C> fl, Function<R, C> fr) {
			return fr.apply(value);
		}
	}
}
