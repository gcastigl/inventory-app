package com.gcastigl.listit.util;

import static com.google.common.base.Strings.nullToEmpty;
import static org.apache.commons.lang3.StringEscapeUtils.escapeJava;
import static org.apache.commons.lang3.StringEscapeUtils.unescapeJava;

public class StringUtil {

	public static String escape(String s) {
		return escapeJava(nullToEmpty(s));
	}

	public static String unescape(String s) {
		return unescapeJava(nullToEmpty(s));
	}
}
