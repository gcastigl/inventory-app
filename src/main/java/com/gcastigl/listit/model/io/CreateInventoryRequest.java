package com.gcastigl.listit.model.io;

import java.time.LocalDateTime;
import java.util.Optional;

public class CreateInventoryRequest {

	private String externalId;
	private String parentExternalId;
	private String text;
	private String comment;
	private boolean completed;
	private int index;
	private String dueDate;
	private String dueTime;
	private LocalDateTime creationDateTime;
	private String reminderType;

	public CreateInventoryRequest(String externalId, String parentExternalId, int index, String text, String comment, boolean completed) {
		this.externalId = externalId;
		this.parentExternalId = parentExternalId;
		this.index = index;
		this.text = text;
		this.comment = comment;
		this.completed = completed;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Optional<String> getComment() {
		return Optional.ofNullable(comment);
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Optional<String> getParentExternalId() {
		return Optional.ofNullable(parentExternalId);
	}

	public void setParentExternalId(String parentExternalId) {
		this.parentExternalId = parentExternalId;
	}

	public boolean isCompleted() {
		return completed;
	}

	public int getIndex() {
		return index;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueTime(String dueTime) {
		this.dueTime = dueTime;
	}

	public String getDueTime() {
		return dueTime;
	}

	public void setCreationDateTime(LocalDateTime creationDateTime) {
		this.creationDateTime = creationDateTime;
	}

	public LocalDateTime getCreationDateTime() {
		return creationDateTime;
	}

	public void reminderType(String reminderType) {
		this.reminderType = reminderType;
	}

	public String getReminderType() {
		return reminderType;
	}
}

