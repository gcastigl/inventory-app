package com.gcastigl.listit.model.io;


import com.gcastigl.listit.model.entity.AppUser;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class AuthenticationToken {

	private String nickname;
	private String displayName;
	private Long expiration;
	private AppUser.ProfileType profileType;
	private String token;

	public AuthenticationToken(String nickname, String displayName, Long expiration, AppUser.ProfileType profileType, String token) {
		this.nickname = nickname;
		this.displayName = displayName;
		this.expiration = expiration;
		this.profileType = profileType;
		this.token = token;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Long getExpiration() {
		return expiration;
	}

	public void setExpiration(Long expiration) {
		this.expiration = expiration;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public AppUser.ProfileType getProfileType() {
		return profileType;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}