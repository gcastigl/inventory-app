package com.gcastigl.listit.model.io;

import com.gcastigl.listit.model.entity.AppUser;
import com.gcastigl.listit.model.entity.AppUser.AuthenticationType;

public class CreateUserRequest {

	private String nickname;
	private String password;
	private String displayName;
	private AppUser.ProfileType profileType;
	private AuthenticationType authType;

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public AppUser.ProfileType getProfileType() {
		return profileType;
	}

	public void setProfileType(AppUser.ProfileType profileType) {
		this.profileType = profileType;
	}

	public void setAuthType(AppUser.AuthenticationType authType) {
		this.authType = authType;
	}

	public AppUser.AuthenticationType getAuthType() {
		return authType;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}
}
