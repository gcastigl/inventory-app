package com.gcastigl.listit.model.io;

import com.gcastigl.listit.model.entity.AppUser;
import com.gcastigl.listit.model.entity.AppUser.AuthenticationType;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class AuthenticationRequest {

	private AuthenticationType type;
	private String username;
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setType(AppUser.AuthenticationType type) {
		this.type = type;
	}

	public AppUser.AuthenticationType getType() {
		return type;
	}
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

}
