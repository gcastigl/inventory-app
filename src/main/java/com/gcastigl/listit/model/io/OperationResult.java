package com.gcastigl.listit.model.io;

import com.gcastigl.listit.util.Either;

import static java.util.Objects.requireNonNull;

public class OperationResult<T> {

	public static <T> OperationResult<T> success(T data) {
		return new OperationResult<>(requireNonNull(data), true);
	}

	public static <T> OperationResult<T> error(Enum<?> type) {
		return new OperationResult<>(type.getClass().getSimpleName(), type.name());
	}

	public static <T> OperationResult<T> fromResult(Either<? extends Enum<?>, T> result) {
		return result.either(OperationResult::error, OperationResult::success);
	}

	private boolean success;
	private String errorType;
	private String errorCode;
	private T data;

	OperationResult() {
	}

	public OperationResult(T data, boolean success) {
		this.data = data;
		this.success = success;
	}

	public OperationResult(String errorType, String errorCode) {
		this.success = false;
		this.errorType = requireNonNull(errorType);
		this.errorCode = requireNonNull(errorCode);
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public T getData() {
		return data;
	}

}
