package com.gcastigl.listit.model.io;

import org.springframework.data.domain.Page;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;

public class GQLPage<T> {

	public static <T> GQLPage<T> empty() {
		return new GQLPage<>(emptyList(), 0);
	}

	public static <T> GQLPage<T> of(Page<T> page) {
		return new GQLPage<>(page);
	}

	private long total;
	private List<T> list;

	public GQLPage(Page<T> page) {
		this.total = page.getTotalElements();
		this.list = requireNonNull(page.getContent());
	}

	public GQLPage(List<T> list, long total) {
		this.total = total;
		this.list = requireNonNull(list);
	}

	public long getTotal() {
		return total;
	}

	public List<T> getList() {
		return list;
	}

}
