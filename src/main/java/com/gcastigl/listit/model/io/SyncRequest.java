package com.gcastigl.listit.model.io;

public class SyncRequest {

	public static SyncRequest createInventoryRequest(CreateInventoryRequest values) {
		SyncRequest request = new SyncRequest();
		request.setExternalId(values.getExternalId());
		request.setCreateInventory(values);
		return request;
	}

	public static SyncRequest deleteInventoryRequest(String externalId) {
		SyncRequest request = new SyncRequest();
		request.setExternalId(externalId);
		request.setDelete(true);
		return request;
	}

	public static SyncRequest updateInventoryRequest(String externalId, CreateInventoryRequest values) {
		SyncRequest request = new SyncRequest();
		request.setExternalId(externalId);
		request.setCreateInventory(values);
		return request;
	}

	private String externalId;
	private CreateInventoryRequest createInventory;
	private boolean delete;

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getExternalId() {
		return externalId;
	}

	public CreateInventoryRequest getCreateInventory() {
		return createInventory;
	}

	public void setCreateInventory(CreateInventoryRequest createInventory) {
		this.createInventory = createInventory;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

	public boolean isDelete() {
		return delete;
	}

}
