package com.gcastigl.listit.model.schema;

import com.gcastigl.listit.model.entity.AppUser;
import com.gcastigl.listit.model.io.CreateInventoryRequest;
import com.gcastigl.listit.model.io.OperationResult;
import com.gcastigl.listit.model.io.SyncRequest;
import com.gcastigl.listit.service.api.AppServices;
import com.gcastigl.listit.service.api.InventoryService;
import graphql.schema.GraphQLInputObjectType;
import graphql.schema.GraphQLList;
import graphql.schema.GraphQLNonNull;
import graphql.schema.GraphQLObjectType;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.gcastigl.listit.model.schema.GQLSchemaTypes.singleResultType;
import static com.gcastigl.listit.util.StringUtil.escape;
import static graphql.Scalars.GraphQLBoolean;
import static graphql.Scalars.GraphQLInt;
import static graphql.Scalars.GraphQLString;
import static graphql.schema.GraphQLArgument.newArgument;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import static graphql.schema.GraphQLInputObjectField.newInputObjectField;
import static graphql.schema.GraphQLInputObjectType.newInputObject;
import static graphql.schema.GraphQLObjectType.newObject;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

@Component
class GQLMutationTypeBuilder {

	@Autowired
	private GQLSchemaTypes types;

	@Autowired
	private AppServices app;

	@Autowired
	private ModelMapper mapper;

	GraphQLObjectType build() {
		GraphQLObjectType inventoryResultType = singleResultType("inventoryResultType", types.inventoryType);
		GraphQLInputObjectType inventoryInputObjectType = newInputObject().name("inventoryInputObjectType")
			.fields(asList(
				newInputObjectField().name("id").type(GraphQLString).build(),
				newInputObjectField().name("parentId").type(GraphQLString).build(),
				newInputObjectField().name("text").type(new GraphQLNonNull(GraphQLString)).build(),
				newInputObjectField().name("comment").type(GraphQLString).build(),
				newInputObjectField().name("completed").type(GraphQLBoolean).build(),
				newInputObjectField().name("index").type(new GraphQLNonNull(GraphQLInt)).build(),
				newInputObjectField().name("dueDate").type(GraphQLString).build(),
				newInputObjectField().name("dueTime").type(GraphQLString).build(),
				newInputObjectField().name("reminderType").type(GraphQLString).build()
			))
			.build();
		return newObject().name("MutationType")
			.field(
				newFieldDefinition()
					.name("create_inventory")
					.type(inventoryResultType)
					.argument(newArgument().name("inventory").type(inventoryInputObjectType).build())
					.dataFetcher(env -> {
						AppUser user = GQLExecutionContext.readUser(env.getContext());
						Map<String, Object> inventory = env.getArgument("inventory");
						String id = (String) inventory.get("id");
						String parentId = (String) inventory.get("parentId");
						String text = escape((String) inventory.get("text"));
						String comment = escape((String) inventory.get("comment"));
						int index = (int) inventory.get("index");
						boolean completed = (boolean) inventory.getOrDefault("completed", false);
						CreateInventoryRequest request = new CreateInventoryRequest(id, parentId, index, text, comment, completed);
						request.setDueDate((String) inventory.get("dueDate"));
						request.setDueTime((String) inventory.get("dueTime"));
						request.reminderType((String) inventory.get("reminderType"));
						return OperationResult.fromResult(app.inventories().create(user, request));
					})
			)
			.field(
				newFieldDefinition()
					.name("update_inventory")
					.type(inventoryResultType)
					.argument(newArgument().name("inventory").type(inventoryInputObjectType).build())
					.dataFetcher(env -> {
						AppUser user = GQLExecutionContext.readUser(env.getContext());
						Map<String, Object> inventory = env.getArgument("inventory");
						String id = (String) inventory.get("id");
						String parentId = (String) inventory.get("parentId");
						String text = escape((String) inventory.get("text"));
						String comment = escape((String) inventory.get("comment"));
						int index = (int) inventory.get("index");
						boolean completed = (boolean) inventory.getOrDefault("completed", false);
						CreateInventoryRequest request = new CreateInventoryRequest(id, parentId, index, text, comment, completed);
						request.setDueDate((String) inventory.get("dueDate"));
						request.setDueTime((String) inventory.get("dueTime"));
						return OperationResult.fromResult(app.inventories().updateOne(user, request));
					})
			)
			.field(
				newFieldDefinition()
					.name("update_inventory_children_index")
					.type(inventoryResultType)
					.argument(asList(
						newArgument().name("parentId").type(GraphQLString).build(),
						newArgument().name("childrenOrder").type(new GraphQLList(GraphQLString)).build()
					))
					.dataFetcher(env -> {
						AppUser user = GQLExecutionContext.readUser(env.getContext());
						String parentId = env.getArgument("parentId");
						List<String> childrenOrder = env.getArgument("childrenOrder");
						return OperationResult.fromResult(app.inventories().updateChildrenIndexes(user, parentId, childrenOrder));
					})
			)
			.field(
				newFieldDefinition()
					.name("delete_inventories").type(new GraphQLList(inventoryResultType))
					.argument(newArgument().name("ids").type(new GraphQLList(GraphQLString)).build())
					.dataFetcher(env -> {
						AppUser user = GQLExecutionContext.readUser(env.getContext());
						List<String> ids = env.getArgument("ids");
						return ids.stream()
								.map(id -> app.inventories().softDeleteOne(user, id, LocalDateTime.now()))
								.map(OperationResult::fromResult)
								.collect(toList());
					})
			)
			.field(
				newFieldDefinition()
					.name("synchronize_inventories").type(new GraphQLList(inventoryResultType))
					.argument(asList(
						newArgument().name("parentId").type(GraphQLString).build(),
						newArgument().name("sync").type(new GraphQLList(newInputObject().name("request")
							.fields(asList(
								newInputObjectField().name("id").type(new GraphQLNonNull(GraphQLString)).build(),
								newInputObjectField().name("delete").type(new GraphQLNonNull(GraphQLBoolean)).build(),
								newInputObjectField().name("create").type(inventoryInputObjectType).build()
							)).build()
						)).build()
					))
					.dataFetcher(env -> {
						AppUser user = GQLExecutionContext.readUser(env.getContext());
						String parentId = env.getArgument("parentId");
						List<Map<String, Object>> sync = env.getArgument("sync");
						List<SyncRequest> syncRequests = sync.stream().map(values -> {
							SyncRequest request = new SyncRequest();
							request.setExternalId((String) values.get("id"));
							request.setDelete((Boolean) values.get("delete"));
							Map<String, Object> createValues = (Map<String, Object>) values.get("create");
							Optional.ofNullable(createValues).ifPresent(map -> {
								CreateInventoryRequest createInventory = new CreateInventoryRequest(
									(String) map.get("id"),
									parentId,
									(Integer) map.get("index"),
									escape((String) map.get("text")),
									escape((String) map.get("comment")),
									(boolean) map.get("completed")
								);
								createInventory.setDueDate((String) map.get("dueDate"));
								createInventory.setDueTime((String) map.get("dueTime"));
								request.setCreateInventory(createInventory);
							});
							return request;
						}).collect(toList());
						return app.inventories().synchronize(user, parentId, syncRequests).stream()
							.map(OperationResult::fromResult)
							.collect(toList());
					})
			)
			.build();
	}


}
