package com.gcastigl.listit.model.schema;

import com.gcastigl.listit.model.entity.AppUser;

public class GQLExecutionContext {

	public static AppUser readUser(Object context) {
		return ((GQLExecutionContext) context).getUser();
	}

	private AppUser user;

	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

}
