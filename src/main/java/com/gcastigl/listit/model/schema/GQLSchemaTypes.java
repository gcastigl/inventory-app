package com.gcastigl.listit.model.schema;

import com.gcastigl.listit.model.entity.AppUser;
import com.gcastigl.listit.model.entity.Inventory;
import com.gcastigl.listit.model.io.GQLPage;
import com.gcastigl.listit.service.api.AppServices;
import graphql.schema.GraphQLEnumType;
import graphql.schema.GraphQLList;
import graphql.schema.GraphQLNonNull;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLOutputType;
import graphql.schema.GraphQLType;
import graphql.schema.GraphQLTypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import static graphql.Scalars.GraphQLBoolean;
import static graphql.Scalars.GraphQLByte;
import static graphql.Scalars.GraphQLInt;
import static graphql.Scalars.GraphQLLong;
import static graphql.Scalars.GraphQLString;
import static graphql.schema.GraphQLArgument.newArgument;
import static graphql.schema.GraphQLEnumType.newEnum;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import static graphql.schema.GraphQLObjectType.newObject;
import static java.util.Arrays.asList;

@Component
public class GQLSchemaTypes {

	static final int PAGE_DEFAULT = 0;
	static final int SIZE_DEFAULT = 10;

	@Autowired
	private AppServices app;

	private GraphQLEnumType profileTypeEnum = newEnum()
		.name("ProfileType")
		.value(AppUser.ProfileType.REGULAR.name())
		.value(AppUser.ProfileType.PREMIUM.name())
		.build();

	GraphQLObjectType userType = newObject()
		.name("User")
		.field(newFieldDefinition().name("nickname").type(new GraphQLNonNull(GraphQLString)))
		.field(newFieldDefinition().name("creationDate").type(new GraphQLNonNull(GraphQLString)))
		.field(newFieldDefinition().name("profileType").type(new GraphQLNonNull(profileTypeEnum)))
		.field(newFieldDefinition().name("displayName").type(GraphQLString))
		.build();

	GraphQLObjectType inventoryType = newObject()
		.name("Inventory")
		.field(newFieldDefinition().name("id").type(GraphQLString)
			.dataFetcher(env -> ((Inventory) env.getSource()).getExternalId())
		)
		.field(newFieldDefinition().name("parentId").type(GraphQLString)
			.dataFetcher(env -> app.inventories().getExternalIdForId(((Inventory) env.getSource()).getParentId()))
		)
		.field(newFieldDefinition().name("text").type(new GraphQLNonNull(GraphQLString)))
		.field(newFieldDefinition().name("comment").type(GraphQLString))
		.field(newFieldDefinition().name("completed").type(new GraphQLNonNull(GraphQLBoolean)))
		.field(newFieldDefinition().name("index").type(new GraphQLNonNull(GraphQLInt))
			.dataFetcher(env -> ((Inventory) env.getSource()).getChildIndex())
		)
		.field(newFieldDefinition().name("dueDate").type(GraphQLString))
		.field(newFieldDefinition().name("dueTime").type(GraphQLString))
		.field(newFieldDefinition().name("reminderType").type(GraphQLString))
		.field(newFieldDefinition().name("children").type(new GraphQLNonNull(pagedType("children_page", new GraphQLTypeReference("Inventory"))))
			.argument(asList(
				newArgument().name("page").type(GraphQLInt).defaultValue(PAGE_DEFAULT).build(),
				newArgument().name("size").type(GraphQLInt).defaultValue(SIZE_DEFAULT).build()
			))
			.dataFetcher(env -> {
				AppUser user = GQLExecutionContext.readUser(env.getContext());
				Inventory item = (Inventory) env.getSource();
				int page = env.getArgument("page");
				int size = env.getArgument("size");
				size = size == 0 ? 1 : size; // TODO: when size = 0 => execute count query only! (more efficient)
				// TODO: get authenticatedUser() from shared settings to prevent re fetching of user information from the DB
				return GQLPage.of(
					app.inventories().findAllChildren(user, item.getExternalId(), new PageRequest(page, size))
				);
			})
		)
		.build();

	static GraphQLObjectType singleResultType(String name, GraphQLOutputType dataType) {
		return newObject().name(name)
			.fields(asList(
				newFieldDefinition().name("success").type(GraphQLBoolean).build(),
				newFieldDefinition().name("errorType").type(GraphQLString).build(),
				newFieldDefinition().name("errorCode").type(GraphQLString).build(),
				newFieldDefinition().name("data").type(dataType).build()
			)).build();
	}

	static GraphQLObjectType pagedType(String name, GraphQLType dataType) {
		return newObject().name(String.format("paged_%s", name))
			.fields(asList(
				newFieldDefinition().name("total").type(GraphQLLong).build(),
				newFieldDefinition().name("list").type(new GraphQLList(dataType)).build()
			))
			.build();
	}

	public static GraphQLObjectType pagedResultType(String name, GraphQLObjectType dataType) {
		return newObject().name(name)
			.fields(asList(
				newFieldDefinition().name("success").type(GraphQLBoolean).build(),
				newFieldDefinition().name("errorType").type(GraphQLString).build(),
				newFieldDefinition().name("errorCode").type(GraphQLString).build(),
				newFieldDefinition().name("page").type(pagedType(name, dataType)).build()
			)).build();
	}

}
