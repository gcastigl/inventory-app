package com.gcastigl.listit.model.schema;

import graphql.schema.GraphQLSchema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class GraphQLSchemaHolder {

	@Autowired
	private GQLQueryTypeBuilder queryTypeBuilder;

	@Autowired
	private GQLMutationTypeBuilder mutationTypeBuilder;

	public GraphQLSchema schema;

	@PostConstruct
	void init() {
		schema = GraphQLSchema.newSchema()
				.query(queryTypeBuilder.build())
				.mutation(mutationTypeBuilder.build())
				.build();
	}
}
