package com.gcastigl.listit.model.schema;

import com.gcastigl.listit.model.entity.AppUser;
import com.gcastigl.listit.model.entity.Inventory;
import com.gcastigl.listit.model.io.GQLPage;
import com.gcastigl.listit.service.api.AppServices;
import com.gcastigl.listit.util.Pages;
import graphql.schema.GraphQLObjectType;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Optional;

import static com.gcastigl.listit.model.schema.GQLSchemaTypes.PAGE_DEFAULT;
import static com.gcastigl.listit.model.schema.GQLSchemaTypes.SIZE_DEFAULT;
import static com.gcastigl.listit.model.schema.GQLSchemaTypes.pagedType;
import static graphql.Scalars.GraphQLInt;
import static graphql.Scalars.GraphQLString;
import static graphql.schema.GraphQLArgument.newArgument;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import static graphql.schema.GraphQLObjectType.newObject;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Objects.isNull;

@Component
class GQLQueryTypeBuilder {

	@Autowired
	private GQLSchemaTypes types;

	@Autowired
	private AppServices app;

	@Autowired
	private ModelMapper mapper;

	GraphQLObjectType build() {
		return newObject().name("QueryType")
			.field(newFieldDefinition()
				.name("user").type(types.userType)
				.dataFetcher(env -> GQLExecutionContext.readUser(env.getContext()))
			)
			.field(newFieldDefinition()
				.name("inventories").type(pagedType("inventories_page", types.inventoryType))
				.argument(asList(
					newArgument().name("id").type(GraphQLString).build(),
					newArgument().name("parentId").type(GraphQLString).build(),
					newArgument().name("page").type(GraphQLInt).defaultValue(PAGE_DEFAULT).build(),
					newArgument().name("size").type(GraphQLInt).defaultValue(SIZE_DEFAULT).build()
				))
				.dataFetcher(env -> {
					AppUser user = GQLExecutionContext.readUser(env.getContext());
					String id = env.getArgument("id");
					if (!isNull(id)) {
						Optional<Inventory> inventory = app.inventories().findOne(user, id);
						return GQLPage.of(Pages.of(inventory.map(Collections::singletonList).orElse(emptyList())));
					} else {
						String parentId = env.getArgument("parentId");
						int page = Optional.ofNullable((Integer) env.getArgument("page")).orElse(PAGE_DEFAULT);
						int size = Optional.ofNullable((Integer) env.getArgument("size")).orElse(SIZE_DEFAULT);
						return GQLPage.of(
							app.inventories().findAllChildren(user, parentId, new PageRequest(page, size))
						);
					}
				})
			)
			.build();
	}

}
