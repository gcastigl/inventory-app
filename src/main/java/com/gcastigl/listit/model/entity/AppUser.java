package com.gcastigl.listit.model.entity;

import static java.util.Objects.requireNonNull;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "appuser", indexes = {
	@Index(name = "unique_email_and_auth", columnList = "nickname, auth_type", unique = true)
})
public class AppUser {

	public enum AuthenticationType {
		USER_AND_PASS, FACEBOOK;
	}

	public enum ProfileType {
		REGULAR, PREMIUM
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "password", nullable = false)
	private String password;

	@Column(name = "auth_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private AuthenticationType authType;

	@Column(name = "nickname", nullable = false)
	private String nickname;

	@Column(name = "creation_date", nullable = false)
	private LocalDateTime creationDate;

	@Column(name = "profile_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private ProfileType profileType;

	@Column(name = "enabled", nullable = false)
	private boolean enabled;

	@Column(name = "display_name")
	private String displayName;

	AppUser() {
		// Required by entity
	}

	public AppUser(ProfileType profileType, String nickname, String password, LocalDateTime creationDate, AuthenticationType authType) {
		this.profileType = requireNonNull(profileType);
		this.nickname = requireNonNull(nickname);
		setPassword(password);
		this.creationDate = requireNonNull(creationDate);
		this.authType = requireNonNull(authType);
		this.enabled = true;
	}

	public Long getId() {
		return id;
	}

	public String getNickname() {
		return nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = requireNonNull(password);
	}

	public ProfileType getProfileType() {
		return profileType;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public boolean hasProfileType(ProfileType profileType) {
		return getProfileType().equals(profileType);
	}

	public boolean isEnabled() {
		return enabled;
	}

	public AuthenticationType getAuthType() {
		return authType;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof AppUser)) {
			return false;
		}
		AppUser other = (AppUser) obj;
		return getNickname().equals(other.getNickname());
	}

	@Override
	public int hashCode() {
		return getNickname().hashCode();
	}
}
