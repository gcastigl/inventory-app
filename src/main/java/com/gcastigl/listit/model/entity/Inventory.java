package com.gcastigl.listit.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

@Entity
@Table(name = "inventory", indexes = {
	@Index(name = "index_all_children", columnList = "owner_id, parent_id, deleted"),
	@Index(name = "index_by_deleted", columnList = "deleted"),
	@Index(name = "unique_external_id_per_parent", columnList = "owner_id, parent_id, externalId", unique = true),
	@Index(name = "index_by_parent", columnList = "parent_id")
})
public class Inventory {

	public static final int TEXT_MAX_LEN = 255;
	public static final int COMMENT_MAX_LEN = 255;
	public static final int EXTERNAL_ID_LEN = 64;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "owner_id", nullable = false)
	private AppUser owner;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id")
	private Inventory parent;

	@Column(name="parent_id", updatable = false, insertable = false)
	private Long parentId;

	@Column(name =  "child_index", nullable = false)
	private int childIndex;

	@Column(name =  "text", length = TEXT_MAX_LEN, nullable = false)
	private String text;

	@Column(name =  "comment", length = COMMENT_MAX_LEN)
	private String comment;

	@Column(name =  "completed", nullable = false)
	private boolean completed;

	@Column(name =  "creation_date_time", nullable = false)
	private LocalDateTime creationDateTime;

	@Column(name =  "update_date_time", nullable = false)
	private LocalDateTime updateDateTime;

	@Column(name =  "deleted", nullable = false)
	private boolean deleted;

	@Column(name =  "externalId", length = EXTERNAL_ID_LEN, nullable = false)
	private String externalId;

	@Column(name =  "due_date")
	private LocalDate dueDate;

	@Column(name =  "due_time")
	private LocalTime dueTime;

	@Column(name =  "reminderType")
	private String reminderType;

	Inventory() {
		// Required by entity
	}

	public Inventory(AppUser owner, Inventory parent, int childIndex, String text, String comment, boolean completed, String external, LocalDateTime creationDateTime) {
		this.owner = checkNotNull(owner);
		this.parent = parent;
		this.childIndex = childIndex;
		setText(text);
		setComment(comment);
		setCompleted(completed);
		setExternalId(external);
		this.creationDateTime = creationDateTime;
		this.updateDateTime = creationDateTime;
	}

	public Long getId() {
		return id;
	}

	public AppUser getOwner() {
		return owner;
	}

	public Inventory getParent() {
		return parent;
	}

	public void setParent(Inventory parent) {
		this.parent = parent;
		this.parentId = isNull(parent) ? null : parent.getId();
	}

	public Long getParentId() {
		return parentId;
	}

	public void setText(String text) {
		checkArgument(text.length() < TEXT_MAX_LEN);
		this.text = requireNonNull(text);
	}

	public String getText() {
		return text;
	}

	public void setComment(String comment) {
		checkArgument(isNull(comment) || comment.length() < COMMENT_MAX_LEN);
		this.comment= comment;
	}

	public String getComment() {
		return comment;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public boolean isCompleted() {
		return completed;
	}

	public LocalDateTime getCreationDateTime() {
		return creationDateTime;
	}

	public int getChildIndex() {
		return childIndex;
	}

	public void setChildIndex(int childIndex) {
		this.childIndex = childIndex;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setExternalId(String externalId) {
		checkArgument(externalId.length() < EXTERNAL_ID_LEN);
		this.externalId = externalId;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueTime(LocalTime dueTime) {
		this.dueTime = dueTime;
	}

	public LocalTime getDueTime() {
		return dueTime;
	}

	public void setUpdateDateTime(LocalDateTime updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public LocalDateTime getUpdateDateTime() {
		return updateDateTime;
	}

	public void setReminderType(String reminderType) {
		this.reminderType = reminderType;
	}

	public String getReminderType() {
		return reminderType;
	}
}
