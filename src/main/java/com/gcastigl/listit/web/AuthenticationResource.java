package com.gcastigl.listit.web;


import com.gcastigl.listit.model.entity.AppUser;
import com.gcastigl.listit.model.io.AuthenticationToken;
import com.gcastigl.listit.model.io.AuthenticationRequest;
import com.gcastigl.listit.model.io.OperationResult;
import com.gcastigl.listit.service.api.AppServices;
import com.gcastigl.listit.service.api.AuthenticationService.AuthenticationErrorType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.Optional;

@Path("/authenticate")
@Component
public class AuthenticationResource {

	@Autowired
	private AppServices app;

	@POST
	@Path("token")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional(readOnly = true)
	public OperationResult<AuthenticationToken> authenticate(final AuthenticationRequest request) {
		try {
			return OperationResult.fromResult(app.authentication().authenticate(request));
		} catch (DisabledException e) {
			return OperationResult.error(AuthenticationErrorType.USER_DISABLED);
		} catch (AuthenticationException ex) {
			return OperationResult.error(AuthenticationErrorType.INVALID_CREDENTIALS);
		}
	}

	@POST
	@Path("renewtoken")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional(readOnly = true)
	public OperationResult<AuthenticationToken> renewToken(@Context ContainerRequestContext securityContext) {
		AppUser user = AuthenticationFilter.authenticatedUser();
		return OperationResult.success(app.authentication().createToken(user, Optional.empty()));
	}
}
