package com.gcastigl.listit.web;


import com.gcastigl.listit.model.entity.AppUser;
import com.gcastigl.listit.service.api.AppServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Collection;
import java.util.Optional;

public class AuthenticationFilter implements ContainerRequestFilter {

	public static final String HEADER_AUTH_TYPE = "Basic";

	public static AppUser authenticatedUser() {
		return (AppUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	@Autowired
	private AppServices app;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		boolean authRequired = requiresAuthentication(requestContext.getUriInfo().getPath().toLowerCase());
		setSecurityContext(requestContext, authRequired);
	}

	private boolean requiresAuthentication(String urlPath) {
		return !urlPath.equals("authenticate/token") && !urlPath.equals("users/create");
	}

	private void setSecurityContext(ContainerRequestContext containerRequest, boolean authRequired) {
		String authorizationHeader = containerRequest.getHeaderString(HttpHeaders.AUTHORIZATION);
		// Check if the HTTP Authorization header is present and formatted
		// correctly
		if (authorizationHeader == null || !authorizationHeader.startsWith(HEADER_AUTH_TYPE + " ")) {
			if (authRequired) {
				throw new NotAuthorizedException("Authorization header must be provided");
			}
			return;
		}
		// Extract the token from the HTTP Authorization header
		String token = authorizationHeader.substring(HEADER_AUTH_TYPE.length()).trim();
		try {
			Optional<AppUser> auth = app.authentication().getUserFromToken(token);
			if (!auth.isPresent()) {
				if (authRequired) {
					throw new NotAuthorizedException("Invalid authorization header");
				}
				return;
			}
			setUserInContext(auth.get(), token);
		} catch (Exception e) {
			containerRequest.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
		}
	}

	private void setUserInContext(AppUser user, String token) {
		Collection<? extends GrantedAuthority> authorities = app.authentication().buildUserDetails(user).getAuthorities();
		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
			user, token, authorities
		);
		SecurityContextHolder.getContext().setAuthentication(authenticationToken);
	}
}
