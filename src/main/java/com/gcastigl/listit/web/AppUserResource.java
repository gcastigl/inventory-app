package com.gcastigl.listit.web;

import com.gcastigl.listit.model.io.AuthenticationToken;
import com.gcastigl.listit.model.io.CreateUserRequest;
import com.gcastigl.listit.model.io.OperationResult;
import com.gcastigl.listit.service.api.AppServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Optional;

@Component
@Path("/users")
public class AppUserResource {

	@Autowired
	private AppServices app;

	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public OperationResult<AuthenticationToken> create(CreateUserRequest request) {
		return app.appusers().create(null, request).either(
				OperationResult::error,
				user -> OperationResult.success(app.authentication().createToken(user, Optional.empty()))
		);
	}
}
