package com.gcastigl.listit.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gcastigl.listit.model.schema.GQLExecutionContext;
import com.gcastigl.listit.model.schema.GraphQLSchemaHolder;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.introspection.IntrospectionQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.gcastigl.listit.web.AuthenticationFilter.authenticatedUser;
import static com.google.common.base.Strings.isNullOrEmpty;
import static java.util.Objects.isNull;

@Component
@Path("/graphql")
public class GQLResource {

	private static final Logger logger = LoggerFactory.getLogger(GQLResource.class);

	private static final String DEFAULT_QUERY_KEY = "query";
	private static final String DEFAULT_VARIABLES_KEY = "variables";
	private static final String DEFAULT_OPERATION_NAME_KEY = "operationName";
	private static final String HEADER_SCHEMA_NAME = "graphql-schema";

	@Autowired
	private GraphQLSchemaHolder schemaHolder;

	private ObjectMapper objectMapper = new ObjectMapper();

	@Path("/schema.json")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object get() throws IOException {
		return execute(IntrospectionQuery.INTROSPECTION_QUERY, null, null);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Object get(
		@QueryParam(DEFAULT_QUERY_KEY) String query,
		@QueryParam(DEFAULT_VARIABLES_KEY) String variables,
		@QueryParam(DEFAULT_OPERATION_NAME_KEY) String operationName,
		@QueryParam(HEADER_SCHEMA_NAME) String graphQLSchemaName) throws IOException
	{
		Map<String, Object> variablesMap = null;
		if (!isNullOrEmpty(variables)) {
			variablesMap = decodeIntoMap(variables);
		}
		return execute(query, variablesMap, operationName);
	}

	@POST
	@Consumes("application/graphql")
	@Produces(MediaType.APPLICATION_JSON)
	public Object post(
		String query,
		@QueryParam(DEFAULT_OPERATION_NAME_KEY) String operationName,
		@HeaderParam(HEADER_SCHEMA_NAME) String graphQLSchemaName) throws IOException
	{
		return execute(query, new HashMap<>(), operationName);
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@SuppressWarnings("unchecked")
	public Object post(
		Map<String, Object> body,
		@HeaderParam(HEADER_SCHEMA_NAME) String graphQLSchemaName) throws IOException
	{
		String query = (String) body.get(DEFAULT_QUERY_KEY);
		String operationName = (String) body.get(DEFAULT_OPERATION_NAME_KEY);
		Map<String, Object> variables = null;
		Object variablesObject = body.get(DEFAULT_VARIABLES_KEY);
		if (variablesObject != null) {
			variables = decodeIntoMap(variablesObject.toString());
		}
		return execute(query, variables, operationName);
	}

	private Object execute(String query, Map<String, Object> variables, String operationName) throws IOException {
		if (isNullOrEmpty(query)) {
			return null;
		}
		if (isNull(variables)) {
			variables = Collections.emptyMap();
		}
		GraphQL graphQL = new GraphQL(schemaHolder.schema);
		GQLExecutionContext context = new GQLExecutionContext();
		context.setUser(authenticatedUser());
		ExecutionResult result = graphQL.execute(query, operationName, context, variables);
		if (!result.getErrors().isEmpty()) {
			return result.getErrors();
		}
		return result.getData();
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> decodeIntoMap(final String variablesParam) throws IOException {
		return objectMapper.readValue(variablesParam, Map.class);
	}

}
